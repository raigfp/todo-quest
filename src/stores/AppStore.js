import Alt from '../alt';
import AppActions from '../actions/AppActions';
import TasksActions from '../actions/TasksActions';
import ToolbarButtons from '../enums/ToolbarButtons';

class AppStore {
  constructor() {
    this.actions = [];
    this.bindAction(TasksActions.selectTask, this.handleSelectTask);
    this.bindAction(AppActions.resetToolbar, this.handleResetToolbar);
  }

  handleSelectTask(task) {
    this.setState({
      task: task,
      actions: [ToolbarButtons.CANCEL, ToolbarButtons.REMOVE]
    });
  }

  handleResetToolbar() {
    this.setState({
      task: null,
      actions: [],
    });
  }
}

export default Alt.createStore(AppStore, 'AppStore');