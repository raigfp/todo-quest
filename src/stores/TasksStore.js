'use strict';

import Alt from '../alt';
import TasksActions from '../actions/TasksActions';

class TasksStore {
  constructor() {
    this.actualTasks = [];
    this.finishedTasks = [];
    this.bindAction(TasksActions.fetchTasks, this.handleFetchTasks);
    this.bindAction(TasksActions.createTask, this.handleCreateTask);
    this.bindAction(TasksActions.checkTask, this.handleCheckTask);
    this.bindAction(TasksActions.removeTask, this.handleRemoveTask);
  }

  handleFetchTasks(tasks) {
    this.setState({
      actualTasks: tasks.filter(currentTask => currentTask.completed == false),
      finishedTasks: tasks.filter(currentTask => currentTask.completed == true)
    });
  }

  handleCreateTask = task => {
    this.setState({
      actualTasks: [task].concat(this.actualTasks)
    });
  };

  handleCheckTask = task => {
    this.setState({
      actualTasks: this.actualTasks.filter(currentTask => currentTask._id !== task._id),
      finishedTasks: [task].concat(this.finishedTasks)
    });
  };

  handleRemoveTask = task => {
    this.setState({
      actualTasks: this.actualTasks.filter(currentTask => currentTask._id !== task._id),
      finishedTasks: this.finishedTasks.filter(currentTask => currentTask._id !== task._id)
    });
  }
}

module.exports = Alt.createStore(TasksStore, 'TasksStore');
