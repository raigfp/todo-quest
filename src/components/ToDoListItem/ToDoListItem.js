'use strict';

import React from 'react-native';
const { View, Text, TouchableOpacity, TouchableWithoutFeedback, Image } = React;

import style from './style';
import TasksActions from '../../actions/TasksActions';

module.exports = class ToDoListItem extends React.Component {
  onPressCheck = () => {
    TasksActions.checkTask(this.props.task);
  };

  onLongPress = () => {
    TasksActions.selectTask(this.props.task);
  };

  renderCheck = () => {
    return (
      <View style={style.checkWrapper}>
        <TouchableOpacity
          onPress={this.onPressCheck}
        >
          <View style={style.check}>{
            this.props.task.completed ?
              <Image source={require('./images/check.png')} style={style.checkImage}/> : undefined
          }</View>
        </TouchableOpacity>
      </View>
    );
   };

  render() {
    const taskStyles = this.props.task.completed ? [style.task, style.taskCompleted] : style.task;
    const task = this.props.task;

    return (
      <View style={taskStyles}>
        <TouchableWithoutFeedback
          onLongPress={this.onLongPress}
        >
          <View style={style.body}>
            <View style={style.scope}>
              <Image source={require('./images/work.png')} style={style.scopeImage} />
            </View>
            <View style={style.text}>
              <Text numberOfLines={1} style={style.title}>{task.title}</Text>
              <Text numberOfLines={1} style={style.description}>{task.title}</Text>
            </View>
            <View style={style.experience}>
              <Text style={style.experienceCount}>{task.experience}</Text>
            </View>
            {this.renderCheck()}
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
};
