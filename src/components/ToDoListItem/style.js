'use strict';
const React = require('react-native');
const { StyleSheet } = React;

module.exports = StyleSheet.create({
  task: {
    flex: 1,
    marginBottom: 5,
    paddingTop: 5,
  },
  taskCompleted: {
    opacity: 0.3,
  },
  body: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 5,
    borderLeftColor: '#03c2f6',
    backgroundColor: '#f4f4f4',
    height: 80,
    padding: 4,
  },
  text: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginLeft: 10,
  },
  title: {
    color: '#1f1f1f',
    fontSize: 16,
  },
  description: {
    color: '#979797',
    fontSize: 14,
  },
  scope: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: 56,
    height: 56,
  },
  scopeImage: {
    width: 44,
    height: 37
  },
  experience: {
    width: 32,
    height: 32,
    borderRadius: 25,
    backgroundColor: '#f75c60',
    marginRight: 20,
    marginLeft: 20,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center'
  },
  experienceCount: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#ffffff'
  },
  checkWrapper: {
    alignSelf: 'center',
  },
  check: {
    borderWidth: 1,
    borderColor: '#bbbbbb',
    width: 32,
    height: 32,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  checkImage: {
    width: 36,
    height: 25
  }
});
