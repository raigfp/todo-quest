'use strict';

import React from 'react-native';
const { View, ScrollView, TextInput, Text, TouchableOpacity, Image } = React;
import style from './style';

import moment from 'moment';
import ruLocale from 'moment/locale/ru';
moment.locale('ru', ruLocale);

import ToDoList from '../ToDoList/ToDoList';
import ExperienceBar from '../ExperienceBar/ExperienceBar';

import TasksActions from '../../actions/TasksActions';

module.exports = class ScreenStart extends React.Component {
  constructor() {
    super();
    const date = moment();

    this.state = {
      newTaskInput: '',
      date: date
    }
  }

  handleNewTaskInputChange(text) {
    this.setState({newTaskInput: text});
  }

  addNewTask = () => {
    TasksActions.createTask({
      title: this.state.newTaskInput,
      completed: false,
      experience: 10,
    });

    this.setState({newTaskInput: ''});
  };

  render() {
    return (
      <View style={style.screen}>
        <View style={style.dateTitle}>
          <Text style={style.date}>{this.state.date.format('LL')}</Text>
        </View>
        <ScrollView>
          <TextInput
            onChangeText={text => this.handleNewTaskInputChange(text)}
            onSubmitEditing={this.addNewTask}
            value={this.state.newTaskInput}
            style={style.newTaskInput}
            placeholder="Добавить задачу..."
          />
          <ToDoList type={'actualTasks'} />
          <View style={style.completedTitle}>
            <Text style={style.completedTitleText}>Выполненные задачи</Text>
          </View>
          <ToDoList type={'finishedTasks'} />
        </ScrollView>
        <TouchableOpacity style={style.addTaskButton}>
          <View style={style.addTaskView}>
            <Image style={style.addTaskImage} source={require('./images/plus.png')}></Image>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
};
