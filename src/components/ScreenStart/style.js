'use strict';
const React = require('react-native');
const { StyleSheet } = React;

module.exports = StyleSheet.create({
  screen: {
    flex: 1,
  },
  dateTitle: {
    height: 50,
    backgroundColor: '#f4f4f4',
    borderBottomWidth: 1,
    borderBottomColor: '#c1c1c1',
    flexDirection: 'row',
    paddingLeft: 30,
  },
  date: {
    flex: 1,
    alignSelf: 'center',
    color: '#1f1f1f',
    fontSize: 16,
  },
  newTaskInput: {
    fontSize: 20,
    color: '#afafaf',
    paddingLeft: 10,
  },
  completedTitle: {
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  completedTitleText: {
    fontSize: 20,
    color: '#afafaf',
  },
  addTaskButton: {
    position: 'absolute',
    bottom: 30,
    right: 30
  },
  addTaskView: {
    width: 80,
    height: 80,
    backgroundColor: '#f75c60',
    borderRadius: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addTaskImage: {
    width: 37,
    height: 37
  },
});
