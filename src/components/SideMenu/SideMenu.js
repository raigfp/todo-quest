import React from 'react-native';
const { ListView, View, Text } = React;

import styles from './styles';

module.exports = class SideMenu extends React.Component {
  constructor() {
    super();
    let dataSource = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 });
    this.dataSource = dataSource.cloneWithRows(['Сегодня', 'Календарь', 'Контексты']);
  }

  render() {
    return (
        <ListView
          style={styles.menu}
          dataSource={this.dataSource}
          renderRow={item => this.renderMenuItem(item)}
        />
    );
  }

  renderMenuItem(item) {
    return (
      <Text>{item}</Text>
    )
  }
};
