'use strict';

import React from 'react-native';
const { View } = React;

import styles from './styles';

module.exports = class ExperienceBar extends React.Component {
  render() {
    return (
      <View style={styles.bar}>
        <View style={styles.barActive}></View>
      </View>
    );
  }
}
