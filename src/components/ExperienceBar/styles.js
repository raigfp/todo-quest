'use strict';

import React from 'react-native'
const { StyleSheet } = React;

module.exports = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    height: 10,
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#ebebeb',
  },
  barActive: {
    width: 150,
    backgroundColor: '#f75c60',
  },
});
