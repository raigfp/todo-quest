import React from 'react-native';

export default React.StyleSheet.create({
  list: {
    paddingLeft: 10,
    paddingRight: 10,
  }
});
