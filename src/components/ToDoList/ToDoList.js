'use strict';

import React from 'react-native';
const { ListView } = React;

import styles from './styles';
import ToDoListItem from '../ToDoListItem/ToDoListItem';
import TasksStore from '../../stores/TasksStore';
import TasksActions from '../../actions/TasksActions';

module.exports = class ToDoList extends React.Component {
  constructor() {
    super();
    this.state = TasksStore.getState();

    let dataSource = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 });
    this.state.dataSource = dataSource.cloneWithRows([]);

    TasksActions.fetchTasks({
      order: {
        _id: 'DESC'
      }
    });
  }

  componentDidMount() {
    TasksStore.listen(this.onChange);
  }

  componentWillUnmount() {
    TasksStore.unlisten(this.onChange);
  }

  onChange = state => {
    this.setState({ dataSource: this.state.dataSource.cloneWithRows(state[this.props.type]) });
  };

  render() {
    return (
      <ListView
        style={styles.list}
        dataSource={this.state.dataSource}
        renderRow={task => this.renderListItem(task)}
      />
    );
  }

  renderListItem(task) {
    return <ToDoListItem task={task} />;
  }
};
