import DB from 'react-native-store';

class TasksSource {
  constructor() {
    DB.model("tasks").then(tasksModel => {
      this.model = tasksModel
    });
  }

  getTasks(filter) {
    return this.model.find(filter);
  }

  createTask(task) {
    return this.model.add(task);
  }

  updateTask(data, id) {
    return this.model.updateById(data, id);
  }

  removeTask(id) {
    return this.model.removeById(id);
  }
}

export default new TasksSource();
