import DB from 'react-native-store';

class ProfileSource {
  constructor() {
    DB.model('profile').then(model => {
        model.experience = model.experience || 0;
        this.model = model;
    });
  }
}

export default new ProfileSource();