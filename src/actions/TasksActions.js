import Alt from '../alt';
import TasksSource from '../sources/TasksSource';

class TasksActions {
  createTask(task) {
    return dispatch => TasksSource.createTask(task).then(task => dispatch(task));
  }

  checkTask(task) {
    return dispatch => TasksSource.updateTask({completed: true}, task._id).then(task => dispatch(task));
  }

  fetchTasks(filter) {
    return dispatch => TasksSource.getTasks(filter).then(tasks => dispatch(tasks));
  }

  selectTask(task) {
    return task;
  }

  removeTask(task) {
    return dispatch => TasksSource.removeTask(task._id).then(task => dispatch(task));
  }
}

export default Alt.createActions(TasksActions);
