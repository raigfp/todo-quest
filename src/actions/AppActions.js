import Alt from '../alt'

class AppActions {
  resetToolbar() {
    return dispatch => dispatch();
  }
}

export default Alt.createActions(AppActions);