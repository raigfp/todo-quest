const ToolbarButtons = Object.freeze({
  REMOVE: 'REMOVE',
  CANCEL: 'CANCEL'
});

export default ToolbarButtons;