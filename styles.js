'use strict';

import React from 'react-native';
const { StyleSheet } = React;

module.exports = StyleSheet.create({
  header: {
    height: 56,
    backgroundColor: '#333333',
  },
  headerActive: {
    backgroundColor: '#03c2f6',
  }
});
