'use strict';

import React, { AppRegistry, DrawerLayoutAndroid, Dimensions, ToolbarAndroid } from 'react-native';

import ScreenStart from './src/components/ScreenStart/ScreenStart';
import SideMenu from './src/components/SideMenu/SideMenu';
import AppStore from './src/stores/AppStore';

import ToolbarButtons from './src/enums/ToolbarButtons';
import TasksActions from './src/actions/TasksActions';
import AppActions from './src/actions/AppActions';

import styles from './styles';

const DRAWER_WIDTH_LEFT = 56;

class ToDoQuest extends React.Component {
  constructor() {
    super();
    this.drawer = {};
    this.actions = {
      [ToolbarButtons.CANCEL]: {
        title: 'Cancel',
        show: 'always',
        icon: require('./images/back.png')
      },
      [ToolbarButtons.REMOVE]: {
        title: 'Remove',
        show: 'always',
        icon: require('./images/trash.png')
      }
    };

    this.state = AppStore.getState();
  }

  componentDidMount() {
    AppStore.listen(this.onChange);
  }

  componentWillUnmount() {
    AppStore.unlisten(this.onChange);
  }

  onChange = state => this.setState(state);

  onActionSelected = actionIndex => {
    switch (this.state.actions[actionIndex]) {
      case ToolbarButtons.REMOVE:
        TasksActions.removeTask(this.state.task);
        AppActions.resetToolbar();
        break;
      case ToolbarButtons.CANCEL:
        AppActions.resetToolbar();
        break;
    }
  };

  getActions = () =>
    // TODO: check what happens if change order in this.actions
    Object.keys(this.actions)
      .filter(action => this.state.actions.indexOf(action) != -1)
      .map(key => this.actions[key]);

  render() {
    const menuWidth = Dimensions.get('window').width - DRAWER_WIDTH_LEFT;
    const toolbarStyle = this.state.task ?
      [styles.header, styles.headerActive] : styles.header;

    return (
      <DrawerLayoutAndroid
        drawerWidth={menuWidth}
        renderNavigationView={() => <SideMenu />}
        ref={drawer => this.drawer = drawer}
      >
        <ToolbarAndroid
          style={toolbarStyle}
          title="ToDo Quest"
          subtitile="Level-up your productivity"
          titleColor="#ffffff"
          actions={this.getActions()}
          navIcon={require('./images/menu.png')}
          onIconClicked={() => this.drawer.openDrawer()}
          onActionSelected={index => this.onActionSelected(index)}
        />
        <ScreenStart />
      </DrawerLayoutAndroid>
    )
  }
}

AppRegistry.registerComponent('ToDoQuest', () => ToDoQuest);
